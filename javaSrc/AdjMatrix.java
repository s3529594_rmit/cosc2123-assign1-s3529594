import java.io.*;
import java.util.*;

/**
 * @author Jeffrey Chan, 2016.
 * @author Jacob Gaffney - s3529594, 2016
 */
public class AdjMatrix <T extends Object> implements FriendshipGraph<T>
{
	HashMap<T, Integer> vLabels = new HashMap<T, Integer>();	// Where we'll be storing our vertex labels
	
	int n = 10;			// Initial size of array
	int count = 0;		// How many vertices we've added
	boolean[][]adjmat;	// Proto-array
	
	// Contructs empty graph.
    public AdjMatrix() {
    	adjmat = new boolean[n][n]; 
    } // end of AdjMatrix()
    
    public void addVertex(T vertLabel) {
    	// Check that the label is new/unique
    	if(vLabels.get(vertLabel) != null) {
    	}
    	else {
    		// Check if th graph needs to be bigger
    		if (count > adjmat.length) {
    			adjmat = growGraph(adjmat);
    		}
    		
    		vLabels.put(vertLabel, count);	// Add our vertex to the Hashmap
    		count++;
    	}
    } // end of addVertex()
	
    public void addEdge(T srcLabel, T tarLabel) {
    	if (vLabels.get(srcLabel) == null || vLabels.get(tarLabel) == null) {
    		throw new IllegalArgumentException();
    	}
    	else {
    		// As edges are not directoinal, set for both variations
    		adjmat[vLabels.get(srcLabel)][vLabels.get(tarLabel)] = true;
    		adjmat[vLabels.get(tarLabel)][vLabels.get(srcLabel)] = true;
    	}
    } // end of addEdge()
	
    public ArrayList<T> neighbours(T vertLabel) {
    	// This works great when I run it manually, but it fails the test for some reason
    	// I can't quite work it out, maybe it's related to the way I'm returning the values
    	// Will have to talk to tutor about this...
        ArrayList<T> neighbours = new ArrayList<T>();
        
        if(vLabels.get(vertLabel) == null ) {
    		throw new IllegalArgumentException();
    	}
        // Find all a vertexes neighbours by checking the state of its row
        if (vLabels.get(vertLabel) != null) {
	        int row = vLabels.get(vertLabel);
	        
	        for (int i = 0; i < adjmat[0].length; i++) {
	        	if (adjmat[row][i] == true) {
	        		neighbours.add(getKeyFromValue(i));
	        	}
	        }
	        return neighbours;
        }
        return null;
    } // end of neighbours() 
    
    public void removeVertex(T vertLabel) {
    	// This function could probably use a bit of work
    	// It'd be nice if we actually cleaned up the graph. got rid of the orphaned rows/columns
    	// Of course, to do that we'd have to move a bunch of values around, and fix our HashMap
    	
    	// We need to remove all the edges
    	if(vLabels.get(vertLabel) == null ) {
    		throw new IllegalArgumentException();
    	}

        for(int i = 0; i < adjmat[0].length; i++) {
        	if (adjmat[vLabels.get(vertLabel)][i] == true)
        	removeEdge(vertLabel, getKeyFromValue(i));
        }

    	vLabels.remove(vertLabel);	// Remove our vertLabel from the HashMap
    	count--;					// Fix our count up
    } // end of removeVertex()
	 
    public void removeEdge(T srcLabel, T tarLabel) {
    	if (vLabels.get(srcLabel) == null || vLabels.get(tarLabel) == null) {
    		throw new IllegalArgumentException();
    	}
    	else {
    		// We have to do both variations as we don't want our graph to be asymmetrical
    		adjmat[vLabels.get(srcLabel)][vLabels.get(tarLabel)] = false;
    		adjmat[vLabels.get(tarLabel)][vLabels.get(srcLabel)] = false;
    	}
    } // end of removeEdges()
	
    public void printVertices(PrintWriter os) {
    	// Check if we have any vertexes, if so print them
    	if (vLabels.isEmpty()){  		
    	}
    	for (T o : vLabels.keySet()) {
    		os.printf(o.toString() + " ");
    	}
    } // end of printVertices()
	
    public void printEdges(PrintWriter os) {
    	// Cycle through the matrix, find all true, print coordinates
    	for (int i = 0; i < adjmat[0].length; i++) {
    		for (int j = 0; j < adjmat[i].length; j++) {
    			if (adjmat[i][j] == true){
    				os.printf(
    						getKeyFromValue(i).toString() + " " + 
    						getKeyFromValue(j).toString() +"\n");
    			}
    		}
    	}
    } // end of printEdges()
    
    // Nice littel algorithm for finding the shortest path
    public int shortestPathDistance(T vertLabel1, T vertLabel2) {
    	Queue<Integer> q = new LinkedList<Integer>();	// New queue to store potential neighbours in 
    	boolean[] visited = new boolean[count];			// New array so we know which vertexes we've visited
    	int steps = 1;									// How long the path was
    	
    	visited[vLabels.get(vertLabel1)] = true;		// We've visited first object
    	q.add(vLabels.get(vertLabel1));					// Add it to the queue
    	
    	// We run this operation until we've exhausted all options
    	while (!q.isEmpty()) {
    		// Check all its neighbours
    		for(T vertex: neighbours(getKeyFromValue(q.element()) ) ) {
    			// First we check if current item in queue is the correct target
    			if (vLabels.get(vertex) == vLabels.get(vertLabel2)) {
    				System.out.println(steps);
    				return steps;
    			}
    			
    			// If not, we all of the connected nodes to the queue
    			if (visited[vLabels.get(vertex)] != true) {
    				visited[vLabels.get(vertex)] = true;
    				q.add(vLabels.get(vertex));
    			}
    		}
    		q.remove(); // Pop the current item off the queue
    		steps++;	// Increase the step counter
    	}
        // if we reach this point, source and target are disconnected
        return disconnectedDist;    	
    } // end of shortestPathDistance()
    
    // Necessary for increasing size of adjacency matrix as we can't use arrayLists
    public boolean[][] growGraph(boolean[][] original){
    	// Whatever you need to do to make the graph bigger
    	int nSize = original[0].length * 2;
    	boolean[][] tempAdj = new boolean[nSize][nSize];
    	
    	// Copy our values across
    	for (int i = 0; i < original[0].length; i++) {
    		for (int j = 0; j < original[0].length; j++) {
    			tempAdj[i][j] = original[i][j];
    		}
    	}
    	
    	// And return our new graph
    	return tempAdj;
    }
    
    // Retrieves key from value, assuming that both are unique
    // This isn't particularly efficient though, consider a second Hashmap
    public T getKeyFromValue(int count) {
    	for (T o : vLabels.keySet()) {
    		if (vLabels.get(o).equals(count)) {
    			return o;
    		}
    	}
        return null;
    } 
} // end of class AdjMatrix
