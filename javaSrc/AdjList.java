import java.io.*;
import java.util.*;

/**
 * Adjacency list implementation for the FriendshipGraph interface.
 * 
 * Your task is to complete the implementation of this class.  You may add methods, but ensure your modified class compiles and runs.
 *
 * @author Jeffrey Chan, 2016.
 * @author Jacob Gaffney s3529594, 2016.
 */
public class AdjList <T extends Object> implements FriendshipGraph<T>
{
    protected HashMap<T, Node> adjListMap = new HashMap();

    // Just linkedlist things
    protected Node lHead;
    protected Node lTail;
    protected int lLength;
        
	// Contructs empty graph.
    public AdjList() {
    	lHead = null;
        lTail = null;
        lLength = 0;
    }
    
    public void addVertex(T vertLabel) {
        // Check to see if we have the vertex
        if(!this.adjListMap.containsKey(vertLabel)) {
            Node newNode = new Node(vertLabel);
            lHead = newNode;
            lTail = newNode;

            lLength++; // Increase length of list

            // Add our node to the HashMap
            this.adjListMap.put(vertLabel, newNode);
        }
        else {}
    } // end of addVertex()
	
    // So this part works but I haven't got anything preventing potential duplicates
    // I should fix that up when I have the chance
    public void addEdge(T srcLabel, T tarLabel) {
    	if (!this.adjListMap.containsKey(srcLabel) || !this.adjListMap.containsKey(tarLabel)){
    		throw new IllegalArgumentException();
    	}
    	else {
    		/*
    		 * Had a bit of trouble with this part
    		 * It's seen a few iterations and it still isn't working as well as I'd like
    		 * Essentially we have to add nodes onto both of the lists involved with this edge
    		 */
    		
    		// Sort out src -> tar
    		// Get to the end of our list so we can add a new edge
			Node tempNode = this.adjListMap.get(srcLabel);
    		while (tempNode.getNext() != null){
    			tempNode = tempNode.getNext();
    		}
    		
    		Node newNode = new Node(tarLabel);
    		newNode.setNext(null);		// newNode will be our new tail, so it won't be pointing anywhere
    		newNode.setPrev(tempNode);	// tempNode is our old tail, where we'll be tacking newNode
    		tempNode.setNext(newNode);	// Update tempNode to point to the new tail
    		
    		// tar -> src
    		// This will mirror the above section, but in the opposite direction
    		tempNode = this.adjListMap.get(tarLabel);
    		newNode = new Node(srcLabel);
    		newNode.setNext(null);
    		
    		// Get to the end of our list so we can add a new edge
    		while (tempNode.getNext() != null){
				tempNode = tempNode.getNext();
			}
    		newNode.setPrev(tempNode);
    		tempNode.setNext(newNode);
    	}
    } // end of addEdge()

    // This works
    public ArrayList<T> neighbours(T vertLabel) {
        ArrayList<T> neighbours = new ArrayList<T>();
        
        // Check for vertex, if it exists find neighbours
        if (!this.adjListMap.containsKey(vertLabel)){
        	throw new IllegalArgumentException();
    	}
    	else {
    		// Setup a temporary node for navigating our adjacency lists
    		Node tempNode = this.adjListMap.get(vertLabel);
        
    		// Iterate through the lists until they're over
	        while (tempNode.lNext != null){
				tempNode = tempNode.getNext();			// This allows our forwards navigation 
				neighbours.add(tempNode.getlValue());	// And this adds our neighbours to the array
			}
    	}
        
        return neighbours;
    } // end of neighbours()
    
    // Had a bit of trouble with this class too, but it's all clear now
    // Was accidentally ignoring the value of the head of each list because of a 
    // silly condition in the while loop
    // Seems to be all sorted now
    public void removeVertex(T vertLabel) {
        if (!this.adjListMap.containsKey(vertLabel)) {
        	throw new IllegalArgumentException();
        }
        else {
        	Node scanNode;	// This navigates for us
        	
        	// Iterate through each of the lists looking for potential edges
        	for (T o: adjListMap.keySet()) {
        		scanNode = adjListMap.get(o);
        		
        		// So long as we're not at the end of the list, we'll be looking for matches
        		while (scanNode != null) {
        			if (scanNode.getlValue().equals(vertLabel)) {
        				// removeEdge should take care of both directions
        				removeEdge(adjListMap.get(o).getlValue(), vertLabel);
        			}
        			scanNode = scanNode.getNext();
        		}
        	}
        	this.adjListMap.remove(vertLabel); // Can the list for the vertex being removed
        }
    } // end of removeVertex()
	
    public void removeEdge(T srcLabel, T tarLabel) {
    	if (!this.adjListMap.containsKey(srcLabel) || !this.adjListMap.containsKey(tarLabel)){
    		throw new IllegalArgumentException();
    	}
    	else {
    		// Sort out src -> tar
    		Node tempNode = this.adjListMap.get(srcLabel);
    		
    		// Find the edge we're removing
    		while(!tempNode.getlValue().equals(tarLabel)){
    			tempNode = tempNode.getNext();
    		}
    		
    		// Checking for head/tail and reassigning pointers where necessary
    		if (tempNode.getPrev() != null)
    			tempNode.getPrev().setNext(tempNode.getNext());
    		if (tempNode.getNext() != null)
    			tempNode.getNext().setPrev(tempNode.getPrev());
    		
    		// Now we sort out tar -> src
    		// This mirrors what we saw above, but in the opposite direction
    		tempNode = this.adjListMap.get(tarLabel);
    		
    		while(!tempNode.getlValue().equals(srcLabel)){
    			tempNode = tempNode.getNext();
    		}
    		
    		if (tempNode.getPrev() != null)
    			tempNode.getPrev().setNext(tempNode.getNext());
    		if (tempNode.getNext() != null)
    			tempNode.getNext().setPrev(tempNode.getPrev());
    	}
    } // end of removeEdges()
    
    public void printVertices(PrintWriter os) {
    	// Check if we have any vertexes, if so print them
    	if (adjListMap.isEmpty()) {
    	}
    	
    	for (T o: adjListMap.keySet()) {
    		os.printf(o.toString() + " ");
    	}
    } // end of printVertices()
    
    // This works
    public void printEdges(PrintWriter os) {
    	// We navigate each list and print out the edges we find
    	for (T o: adjListMap.keySet()) {
    		Node tempNode = adjListMap.get(o).lNext;
    		
    		while (tempNode != null) {
    			os.printf(adjListMap.get(o).getlValue().toString() + " " + tempNode.getlValue().toString() +"\n");
    			tempNode = tempNode.getNext();
    		}
    	}
    } // end of printEdges()
    
    public int shortestPathDistance(T vertLabel1, T vertLabel2) {
    	Queue<T> q = new LinkedList<T>();					// New queue to store potential neighbours in
    	HashMap<T, Integer> visitedMap = new HashMap();		// A map of where we've visited
    	boolean[] visited = new boolean[adjListMap.size()];	// New array so we know which vertexes we've visited
    	int steps = 1;										// How long the path was
    	int count = 0;										// For use with the visitedMap
    	
    	// Add each of the vertexes to the visited map
    	for (T vertex: adjListMap.keySet()) {
    		visitedMap.put(adjListMap.get(vertex).getlValue(), count);
    		count++;
    	}
    	// Make sure we initialize this
    	visited[visitedMap.get(vertLabel1)] = true;	// We've visited first object
    	q.add(vertLabel1);							// Add it to the queue
    	
    	// We run this operation until we've exhausted all options
    	while (!q.isEmpty()) {
    		// Check all its neighbours
    		for(T vertex: neighbours(q.element()) ) {
    			// First we check if current item in queue is the correct target
    			if (adjListMap.get(vertex) == adjListMap.get(vertLabel2)) {
    				return steps;
    			}
    			// If not, we add all of the connected nodes to the queue
    			if (visited[visitedMap.get(vertex)] != true) {
    				visited[visitedMap.get(vertex)] = true;
    				q.add(vertex);
    			}
    		}
    		q.remove(); // Pop the current item off the queue
    		steps++;	// Increase the step counter
    	}
        return disconnectedDist; // If we reach this point, source and target are disconnected    	
    } // end of shortestPathDistance()


    // Now we're talking manual Node creation
    // This is our basic Node Object, our nodes makes up our lists
    private class Node {
        private T lValue;
        private Node lNext;
        private Node lPrev;

        public Node(T vertLabel) {
            lValue = vertLabel;
            lNext = null;
            lPrev = null;
        }
        
        // Getters and setters
        public T getlValue() { return lValue; }
        public Node getNext() { return lNext; }
        public Node getPrev() { return lPrev; }
        public void setValue(T vertLabel) { lValue = vertLabel; }
        public void setNext(Node next) { lNext = next; }
        public void setPrev(Node prev) { lPrev = prev; }
    }
} // end of class AdjList