#!usr/bin/env python

import sys
import random
from time import time

# Generate data for COSC2123 A1
# Jacob Gaffney s3529594

# Our two arguments
try:
    # Type of data being generated
    argument = str(sys.argv[1])
    # Amount of vertices
    vertex  = int(sys.argv[2])
    # Density of the graph
    density     = float(sys.argv[3]) / 100

except IndexError, e:
    pass

# Time in milliseconds
milli = time() * 1000

if argument == "ae":
    # Output file is generated from arguments
    output_file = "{}_{}.in".format(vertex, density)

    # How many edges do we need
    edge = int(density * vertex * vertex)
    print("Creating data with {} vertices and {} edges ({} density)...".format(vertex, edge, density * 100))

    # This will be an array of previous edges so we don't retread our steps
    edges = []

    # Generate our edges!
    for i in range(edge):
        startV = random.randint(0, vertex) # Start and end of vertex
        endV = random.randint(0, vertex) # pair for our edge
        # We don't want repeats of edges because our implementation does not care for that
        while endV == startV or (startV, endV) in edges:
            endV = random.randint(0, edge)
        # Add it
        edges.append((startV, endV))

    # Print some messages so we know where we're up to if there's an error
    print("\nSort took: {}ms".format( time() * 1000 - milli ))

    # Sort our edges so our files look pretty
    edges.sort(key=lambda x: x[0] * vertex + x[1])

    # Write our output to a file
    print("Write took: {}ms".format( time() * 1000 - milli ))
    with open(output_file, "w") as out:
        for e in edges:
            out.write("%d %d\n" % e)
else:
    # Output file is generated from arguments
    output_file = "{}_{}.in".format(argument, vertex)

    print("Creating data with {} vertices".format(vertex))

    vertices = []
    i = 1

    while i < vertex:
        vertices.append(i)
        i += 1

    with open(output_file, "w") as out:
        for v in vertices:
            out.write("AV %d\n" % v)
        
        if argument == "rv":
            for v in reversed(vertices):
                out.write("RV %d\n" % v)

# Wrap up and total time
print("Total in: {}ms".format( time() * 1000 - milli ))
