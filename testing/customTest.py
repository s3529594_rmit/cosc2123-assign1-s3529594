#
# Script to perform automated testing for assignment 1 of AA, 2016 semester 2
# Modified code for use with unofficial tests based around density and vertex addition/removal
# Jeffrey Chan, 2016
#

import string
import csv
import sets
import getopt
import os
import os.path
import platform
import re
import sys
import subprocess as sp


def main():

    # process command line arguments
    try:
        # option list
        sOptions = "vf:"
        # get options
        optList, remainArgs = getopt.gnu_getopt(sys.argv[1:], sOptions)
    except getopt.GetoptError, err:
        print >> sys.stderr, str(err)
        usage(sys.argv[0])

    bVerbose = False
    bInputFile = False
    sInputFile = ""

    for opt, arg in optList:
        if opt == "-v":
            bVerbose = True
        elif opt == "-f":
            bInputFile = True
            sInputFile = arg
        else:
            usage(sys.argv[0])


    if len(remainArgs) < 3:
        usage(sys.argv[0])


    # code directory
    sCodeDir = remainArgs[0]
    # which implementation to test (see MultiTester.java for the implementation strings)
    sImpl = remainArgs[1]
    # set of input files that contains the operation commands
    lsInFile = remainArgs[2:]
    
    
    # check implementatoin
    setValidImpl = set(["adjlist", "adjmat", "sample"])
    if sImpl not in setValidImpl:
        print >> sys.stderr, sImpl + " is not a valid implementation name."
        sys.exit(1)


    # compile the skeleton java files
    sClassPath = "-cp .:jopt-simple-5.0.2.jar:sample.jar"
    sOs = platform.system()
    if sOs == "Windows":
        sClassPath = "-cp .;jopt-simple-5.0.2.jar;sample.jar"

    sCompileCommand = "javac " + sClassPath + " *.java"
    print sCompileCommand
    sExec = "GraphTester"

    # whether executable was compiled and constructed
    bCompiled = False

    sOrigPath = os.getcwd()
    os.chdir(sCodeDir)

    # compile
    proc = sp.Popen([sCompileCommand], shell=True, stderr=sp.PIPE)
    proc = sp.Popen(sCompileCommand, shell=True, stderr=sp.PIPE)
    (sStdout, sStderr) = proc.communicate()
    print sStderr

    # check if executable was constructed
    if not os.path.isfile(sExec + ".class"):
        print >> sys.stderr, sExec + ".java didn't compile successfully."
    else:
        bCompiled = True


    # variable to store the number of tests passed
    passedNum = 0
    vTestPassed = [False for x in range(len(lsInFile))]
    print ""

    if bCompiled:
        # loop through each input test file
        for (j, sInLoopFile) in enumerate(lsInFile):
            sInFile = os.path.join(sOrigPath, sInLoopFile);
            sTestName = os.path.splitext(os.path.basename(sInFile))[0]
            #sOutputFile = os.path.join(sCodeDir, sTestName + "-" + sImpl + ".out")
            sVertOutputFile = os.path.join(sTestName + "-" + sImpl + ".vert.out")
            sVertExpectedFile = os.path.splitext(sInFile)[0] + ".vert.exp"
            
            # check if expected files exist
            if not os.path.isfile(sVertExpectedFile):
                print >> sys.stderr, sVertExpectedFile + " is missing."
                continue
            

            sCommand = os.path.join("java " + sClassPath + " " + sExec + " " + sImpl + " " + sVertOutputFile)
            if bInputFile:
                sCommand = os.path.join("java " + sClassPath + " " + sExec + " -f" + sInputFile + " " + sImpl + " " + sVertOutputFile)
            
            
            # following command used by my dummy code to test possible output (don't replace above)
#                 lCommand = os.path.join(sCodeDir, sExec + " " + sExpectedFile + ".test")
            if bVerbose:
                print "Testing: " + sCommand
            with open(sInFile, "r") as fIn:
                proc = sp.Popen(sCommand, shell=True, stdin=fIn, stderr=sp.PIPE)
                #proc = sp.Popen(sCommand, shell=True, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)

                #(sStdout, sStderr) = proc.communicate("a hello\np\nq")
                (sStdout, sStderr) = proc.communicate()

                if sStderr == None:
                    print >> sys.stderr, "Cannot execute " + sInFile
                    print >> sys.stderr, sStderr
                else:
                    z = 12
                    # compare expected with output
                    #bVertPassed = vertEvaluate(sVertExpectedFile, sVertOutputFile)
                    #if bVertPassed:
                        #passedNum += 1
                        #vTestPassed[j] = True
                    #else:
                        # print difference if failed
                     #   if bVerbose:
                      #      if not bVertPassed:
                       #         print >> sys.stderr, "Difference between vertices output and expected:"
                        #        proc = sp.Popen("diff -y " + sVertOutputFile + " " + sVertExpectedFile, shell=True)
                         #       proc.communicate()
                          #      print >> sys.stderr, ""
                                                                        

    # change back to original path
    os.chdir(sOrigPath)

    print "SUMMARY: " + sExec + " has passed " + str(passedNum) + " out of " + str(len(lsInFile)) + " tests."
    print "PASSED: " + ", ".join([str(x+1) for (x,y) in enumerate(vTestPassed) if y == True]) + "\n"




#######################################################################################################


def vertEvaluate(sExpectedFile, sOutputFile):
    """
    Evaluate if the output is the same as expected input for the vertices operation.
    """

    lExpMatches = []
    lActMatches = []
    sDelimiter = ", "

    with open(sExpectedFile, "r") as fExpected:
        # should only be one line
        for sLine in fExpected:
            # space delimiter
            sLine1 = sLine.strip()
            lFields = re.split("[\t ]*[,|\|]?[\t ]*", sLine1)
            lExpMatches.extend(lFields)


    with open(sOutputFile, "r") as fOut:
        # should only be one line
        for sLine in fOut:
            # space delimiter
            sLine1 = sLine.strip()

            # if line is empty, we continue (this also takes care of extra newline at end of file)
            if len(sLine1) == 0:
                continue
            # should be space-delimited, but in case submissions use other delimiters
            lFields = re.split("[\t ]*[,|\|]?[\t ]*", sLine1)
            lActMatches.extend(lFields)
            
    setExpMatches = sets.Set(lExpMatches)
    setActMatches = sets.Set(lActMatches)
    


    # if there are differences between the sets
    if len(setExpMatches.symmetric_difference(setActMatches)) > 0:
        return False

    # passed
    return True

def usage(sProg):
    print >> sys.stderr, sProg + "[-v] <code directory> <name of implementation to test> <list of test input files>"
    sys.exit(1)

if __name__ == "__main__":
    main()
